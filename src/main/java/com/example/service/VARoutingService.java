package com.example.service;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.bean.BaseVA;
import com.example.bean.VASkill;
import com.example.config.RoutingConfig;
import com.example.config.WatsonConfig;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.assistant.v2.Assistant;
import com.ibm.watson.assistant.v2.model.MessageResponse;
import com.ibm.watson.assistant.v2.model.RuntimeIntent;

@Service
public class VARoutingService extends BaseVA {

  private static final Logger logger = LogManager.getLogger(VARoutingService.class.getName());

  private final Map<String, VASkill> skillMap = new ConcurrentHashMap<>();

  @Autowired
  private RoutingConfig routingConfig;

  @PostConstruct
  public void init() {
    // create routing skill service
    IamAuthenticator authenticator = new IamAuthenticator(routingConfig.getApiKey());
    assistant = new Assistant(routingConfig.getVersion(), authenticator);
    assistant.setServiceUrl(routingConfig.getAssistantUrl());
    assistantId = routingConfig.getAssistantId();

    // create all skill services
    Map<String, WatsonConfig> skillConfigMap = routingConfig.getSkills();
    for (Entry<String, WatsonConfig> skill : skillConfigMap.entrySet()) {
      WatsonConfig skillConfig = skill.getValue();
      VASkill vaSkill = new VASkill(skillConfig);
      skillMap.put(skill.getKey(), new VASkill(skillConfig));

      String nextSkillName = skillConfig.getNextSkill();
      if (nextSkillName != null && !"".equals(nextSkillName)
          && skillConfigMap.containsKey(nextSkillName)) {
        vaSkill.setNextVASkill(skillMap.get(nextSkillName));
      }
    }

    logger.debug(routingConfig);
  }


  public String routing(String text, HttpSession httpSession) {
    return this.routing(text, httpSession.getId());
  }

  public String routing(String text, String httpSessionId) {

    if (USER_SKILL_MAP.containsKey(httpSessionId)) {
      VASkill vaSkill = USER_SKILL_MAP.get(httpSessionId);
      return vaSkill.chatting(text, httpSessionId);
    }

    MessageResponse response = this.doChat(text, httpSessionId);
    
    if(response == null) {
      return SESSION_TIMEOUT; 
    }
    
    if (this.isEnding(response)) {
      this.deleteSession(httpSessionId);
      this.setEndingWord(response);
      logger.debug("unknown intent in router: {}", response);
      return this.endingWord;
    }

    List<RuntimeIntent> intents = response.getOutput().getIntents();
    for (RuntimeIntent intent : intents) {
      if (skillMap.containsKey(intent.intent())) {
        VASkill vaSkill = skillMap.get(intent.intent());
        USER_SKILL_MAP.put(httpSessionId, vaSkill);
        return vaSkill.chatting(text, httpSessionId);
      }
    }

    return UNKNOWN_INTENT;
  }
}
