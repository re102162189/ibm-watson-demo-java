package com.example.bean;

import java.util.List;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.example.config.WatsonConfig;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.assistant.v2.Assistant;
import com.ibm.watson.assistant.v2.model.MessageResponse;
import com.ibm.watson.assistant.v2.model.RuntimeResponseGeneric;

public class VASkill extends BaseVA {

  private final Logger logger = LogManager.getLogger(VASkill.class.getName());

  private VASkill nextVASkill;

  public VASkill(WatsonConfig config) {
    IamAuthenticator authenticator = new IamAuthenticator(config.getApiKey());
    assistant = new Assistant(config.getVersion(), authenticator);
    assistant.setServiceUrl(config.getAssistantUrl());
    assistantId = config.getAssistantId();
    logger.info(config);
  }

  public String chatting(String text, String httpSessionId) {

    MessageResponse response = this.doChat(text, httpSessionId);
    logger.info("response: {}", response);

    if(response.getOutput() == null) {
      return SESSION_TIMEOUT;
    }

    if (this.isEnding(response)) {
      this.deleteSession(httpSessionId);
      USER_SKILL_MAP.remove(httpSessionId);
      this.setEndingWord(response);
      logger.debug("unknown intent in skill: {}", response);
      if (this.nextVASkill == null) {
        return this.endingWord;
      } else {
        logger.debug("chain next skill: {}", nextVASkill.assistantId);
        USER_SKILL_MAP.put(httpSessionId, nextVASkill);
        return nextVASkill.chatting(text, httpSessionId);
      }
    }

    String output = "";
    List<RuntimeResponseGeneric> generics = response.getOutput().getGeneric();
    if (generics != null && !generics.isEmpty()) {
      StringBuilder texts = new StringBuilder();
      for (RuntimeResponseGeneric generic : generics) {
        texts.append(generic.text()).append("\n");
      }
      output = texts.toString();
      logger.debug("output: {}", output);
    }

    return output;
  }

  public String chatting(String text, HttpSession httpSession) {
    return this.chatting(text, httpSession.getId());
  }

  public VASkill getNextVASkill() {
    return nextVASkill;
  }

  public void setNextVASkill(VASkill nextVASkill) {
    this.nextVASkill = nextVASkill;
  }
}
