package com.example.bean;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.ibm.cloud.sdk.core.service.exception.NotFoundException;
import com.ibm.cloud.sdk.core.service.exception.RequestTooLargeException;
import com.ibm.cloud.sdk.core.service.exception.ServiceResponseException;
import com.ibm.watson.assistant.v2.Assistant;
import com.ibm.watson.assistant.v2.model.CreateSessionOptions;
import com.ibm.watson.assistant.v2.model.DeleteSessionOptions;
import com.ibm.watson.assistant.v2.model.MessageInput;
import com.ibm.watson.assistant.v2.model.MessageOptions;
import com.ibm.watson.assistant.v2.model.MessageResponse;
import com.ibm.watson.assistant.v2.model.RuntimeEntity;
import com.ibm.watson.assistant.v2.model.RuntimeIntent;
import com.ibm.watson.assistant.v2.model.RuntimeResponseGeneric;
import com.ibm.watson.assistant.v2.model.SessionResponse;

public abstract class BaseVA {

  private final Logger logger = LogManager.getLogger(BaseVA.class.getName());

  protected static final String UNKNOWN_INTENT = "unknown intent";

  protected static final String SESSION_TIMEOUT = "session timeout";

  protected Assistant assistant;

  protected String assistantId;

  protected String endingWord;

  // share use between skills and router
  protected static final Map<String, VASkill> USER_SKILL_MAP = new ConcurrentHashMap<>();

  // TODO using share session if load balance requesting
  private final Map<String, SessionResponse> httpToWatsonSessionMap = new ConcurrentHashMap<>();

  protected SessionResponse generateSession(String httpSessionId) {

    if (httpToWatsonSessionMap.containsKey(httpSessionId)) {
      return httpToWatsonSessionMap.get(httpSessionId);
    }

    CreateSessionOptions createSessionOptions =
        new CreateSessionOptions.Builder(assistantId).build();
    SessionResponse sessionResponse =
        assistant.createSession(createSessionOptions).execute().getResult();
    httpToWatsonSessionMap.put(httpSessionId, sessionResponse);

    return sessionResponse;
  }

  protected void deleteSession(String httpSessionId) {
    if (!httpToWatsonSessionMap.containsKey(httpSessionId)) {
      return;
    }
    SessionResponse sessionResponse = httpToWatsonSessionMap.get(httpSessionId);
    String sessionId = sessionResponse.getSessionId();
    DeleteSessionOptions options = new DeleteSessionOptions.Builder(assistantId, sessionId).build();
    assistant.deleteSession(options).execute();
    httpToWatsonSessionMap.remove(httpSessionId);
    logger.info("removed assistance session id: " + sessionId);
  }

  protected MessageResponse doChat(String text, HttpSession httpSession) {
    return this.doChat(text, httpSession.getId());
  }

  protected MessageResponse doChat(String text, String httpSessionId) {

    try {
      String sessionId = this.generateSession(httpSessionId).getSessionId();
      MessageInput input = new MessageInput.Builder().messageType("text").text(text).build();
      MessageOptions options =
          new MessageOptions.Builder(assistantId, sessionId).input(input).build();
      return assistant.message(options).execute().getResult();
    } catch (NotFoundException e) {
      logger.error("NotFoundException {}:{}", e.getStatusCode(), e.getMessage());
    } catch (RequestTooLargeException e) {
      logger.error("RequestTooLargeExceptione {}:{}", e.getStatusCode(), e.getMessage());
    } catch (ServiceResponseException e) {
      logger.error("ServiceResponseException {}:{}", e.getStatusCode(), e.getMessage());
    }

    this.httpToWatsonSessionMap.remove(httpSessionId);
    return new MessageResponse();
  }

  protected void setEndingWord(MessageResponse response) {
    List<RuntimeResponseGeneric> generics = response.getOutput().getGeneric();
    if (generics != null && !generics.isEmpty()) {
      StringBuilder outputs = new StringBuilder();
      for (RuntimeResponseGeneric generic : generics) {
        outputs.append(generic.text()).append("\n");
      }
      this.endingWord = outputs.toString();
    }
  }

  protected boolean isEnding(MessageResponse response) {

    boolean isEnding = false;

    if (response.getOutput() == null) {
      return isEnding;
    }

    List<RuntimeIntent> intents = response.getOutput().getIntents();
    List<RuntimeEntity> entities = response.getOutput().getEntities();
    if ((intents == null || intents.isEmpty()) && (entities == null || entities.isEmpty())) {
      isEnding = true;
    }

    return isEnding;
  }
}
