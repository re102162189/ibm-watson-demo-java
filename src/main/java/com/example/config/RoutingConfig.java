package com.example.config;

import java.util.HashMap;
import java.util.Map;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "ibm-watson-routing")
@EnableConfigurationProperties
public class RoutingConfig extends WatsonConfig {

  private Map<String, WatsonConfig> skills = new HashMap<>();

  public Map<String, WatsonConfig> getSkills() {
    return new HashMap<>(skills);
  }

  public void setSkills(Map<String, WatsonConfig> skills) {
    this.skills = skills;
  }

  @Override
  public String toString() {
    return "RoutingConfig [skills=" + skills + "]";
  }

}