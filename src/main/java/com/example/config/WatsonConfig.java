package com.example.config;

public class WatsonConfig {
  
  protected String version;

  protected String apiKey;

  protected String assistantId;

  protected String assistantUrl;
  
  protected String nextSkill;
  
  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getApiKey() {
    return apiKey;
  }

  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }

  public String getAssistantId() {
    return assistantId;
  }

  public void setAssistantId(String assistantId) {
    this.assistantId = assistantId;
  }

  public String getAssistantUrl() {
    return assistantUrl;
  }

  public void setAssistantUrl(String assistantUrl) {
    this.assistantUrl = assistantUrl;
  }

  public String getNextSkill() {
    return nextSkill;
  }

  public void setNextSkill(String nextSkill) {
    this.nextSkill = nextSkill;
  }
}
