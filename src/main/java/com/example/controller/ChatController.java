package com.example.controller;

import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import com.example.bean.Message;
import com.example.service.VARoutingService;

@Controller
public class ChatController {

  private static final Logger logger = LoggerFactory.getLogger(ChatController.class);

  private static final String CHATPATH = "/chat-room";

  @Autowired
  private SimpMessageSendingOperations messagingTemplate;

  @Autowired
  private VARoutingService routingService;

  @GetMapping(value = "/chat/welcome")
  public String chatroom(Model model, HttpSession session) {
    model.addAttribute("roomId", session.getId());
    return "index";
  }

  @MessageMapping("/chat/{roomId}/sendMessage")
  public void sendMessage(@DestinationVariable String roomId, @Payload Message chatMessage,
      SimpMessageHeaderAccessor headerAccessor) {

    logger.info("httpSession: {}", headerAccessor);

    String response =
        routingService.routing(chatMessage.getContent(), headerAccessor.getSessionId());
    chatMessage.setContent(response);

    messagingTemplate.convertAndSend(String.format("%s/%s", CHATPATH, roomId), chatMessage);
  }

  @MessageMapping("/chat/{roomId}/addUser")
  public void addUser(@DestinationVariable String roomId, @Payload Message chatMessage,
      SimpMessageHeaderAccessor headerAccessor) {

    logger.info("add user: {}", chatMessage.getSender());

    headerAccessor.getSessionAttributes().put("name", chatMessage.getSender());
    messagingTemplate.convertAndSend(String.format("%s/%s", CHATPATH, roomId), chatMessage);
  }
}
