package com.example.controller;

import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.example.service.VARoutingService;

@RestController
@RequestMapping(value = "/rest/va")
public class VARestController {

  private static final Logger logger = LogManager.getLogger(VARestController.class.getName());

  @Autowired
  private VARoutingService routingService;
  
  @GetMapping(value = "/chat")
  public String routing(@RequestParam String text, HttpSession httpSession) {
    
    logger.debug(httpSession.getId());
    logger.debug(text);

    return routingService.routing(text, httpSession);
  }
}
